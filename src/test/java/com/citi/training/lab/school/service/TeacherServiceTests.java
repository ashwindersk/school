package com.citi.training.lab.school.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.citi.training.lab.school.model.Student;
import com.citi.training.lab.school.model.Teacher;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TeacherServiceTests {

    @Autowired 
    private TeacherService teacherService;

    @Test
    public void test_save_sanityCheck(){
        Student s1 = new Student();
        Student s2 = new Student();

        s1.setName("Ash");
        s1.setAge(15);
        s1.setYearGroup(10);
        s2.setName("Winder");
        s2.setAge(15);
        s2.setYearGroup(10);

        Teacher teacher = new Teacher();
        teacher.setAge(30);
        teacher.setName("Ashley Tisdale");
        List<Student> listOfStudents = new ArrayList<Student>();
        Collections.addAll(listOfStudents, s1, s2);
        teacher.setStudents(listOfStudents);

        Teacher savedTeacher = teacherService.save(teacher);

        assert(savedTeacher != null);
        
    }

    @Test
    public void test_listStudentsById(){
        String id = "5f75de703b89915957807ebc";
        List<Student> listOfStudents = teacherService.listStudentsById(id);
        assert(listOfStudents.size() == 2);
        
    }


}
