package com.citi.training.lab.school.service;


import com.citi.training.lab.school.model.Student;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StudentServiceTests {

    @Autowired
    private StudentService studentService;

    @Test
    public void test_save_sanityCheck(){
        Student student = new Student();
        student.setAge(15);
        student.setName("Ashwinder Khurana");
        student.setYearGroup(10);

        Student savedStudent = studentService.save(student);
        
    }

    @Test
    public void test_findAll_sanityCheck(){
        assert(studentService.findAll().size() > 0);
    }

}
