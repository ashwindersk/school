package com.citi.training.lab.school.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.citi.training.lab.school.doa.StudentMongoRepo;
import com.citi.training.lab.school.model.Student;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class StudentServiceMockingTests {

    @Autowired
    private StudentService studentService;

    @MockBean
    private StudentMongoRepo mockStudentRepo;

    @Test
    public void test_studentRepo_save(){
        Student testStudent = new Student();
        testStudent.setName("Mockwinder");
        testStudent.setAge(15);
        testStudent.setYearGroup(10);

        //Tell mockRepo that employeeService is going to call save()
        //when it does, return testEmployee object
        when(mockStudentRepo.save(testStudent)).thenReturn(testStudent);
        studentService.save(testStudent);
    }

    @Test
    public void test_studentRepo_findAll(){
        Student testStudent = new Student();
        testStudent.setName("Mock");
        testStudent.setAge(15);
        testStudent.setYearGroup(10); 
        List<Student> listOfStudents = new ArrayList<Student>();
        listOfStudents.add(testStudent);
        when(mockStudentRepo.findAll()).thenReturn(listOfStudents);
        assert(studentService.findAll().size() > 0);

    }
    
}
