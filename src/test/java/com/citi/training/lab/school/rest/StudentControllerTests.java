package com.citi.training.lab.school.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import com.citi.training.lab.school.model.Student;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class StudentControllerTests {

    // This will "run" the application and use it from a client
    // from a port that isnt the same as the normal one

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test_save_findAll(){
        Student student = new Student();
        student.setAge(10);
        student.setName("Rest Khurana");
        student.setYearGroup(6);

        ResponseEntity<Student> response = restTemplate.postForEntity("/v1/student", 
                                                                       student, 
                                                                       Student.class);

        assertEquals(response.getStatusCode(), HttpStatus.CREATED);

        
		ResponseEntity<List<Student>> findAllResponse = restTemplate.exchange("/v1/student",
                                                                              HttpMethod.GET, 
                                                                                         null, 
                                                                              new ParameterizedTypeReference<List<Student>>(){});
                                                                                  
                                                                            
        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
    }
}
