package com.citi.training.lab.school.rest;

import com.citi.training.lab.school.service.StudentService;


import java.util.List;

import com.citi.training.lab.school.model.Student;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/student")
public class StudentController {

    private static final Logger LOG = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @RequestMapping(method=RequestMethod.GET)
    public List<Student> findAll(){
        LOG.debug("findAll() request received");
        return studentService.findAll();
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Student> save(@RequestBody Student student){
        LOG.debug("Save student request received");
        return new ResponseEntity<Student>(studentService.save(student), HttpStatus.CREATED);
    }

    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable String id){
        LOG.debug("Delete student request received");
        try {
            studentService.deleteById(id);    
        } catch (Exception e ) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Student> update(@PathVariable String id,
                                       @RequestBody Student student ){
        LOG.debug("Update student request received");
      
        
        return new ResponseEntity<Student>(studentService.update(id, student),HttpStatus.OK);
    }
    
}
