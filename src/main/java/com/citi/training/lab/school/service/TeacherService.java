package com.citi.training.lab.school.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.citi.training.lab.school.doa.TeacherMongoRepo;
import com.citi.training.lab.school.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeacherService {

    @Autowired
    private TeacherMongoRepo mongoRepo;

    public List<Teacher> findAll(){
        return mongoRepo.findAll();
    }

    public boolean existsById(String id){
        return mongoRepo.existsById(id);
    }

    public Teacher save(Teacher teacher){
        return mongoRepo.save(teacher);
    }

    public List<Student> listStudentsById(String id){
        try{
            Optional<Teacher> teacher = mongoRepo.findById(id);
            return teacher.get().getStudents();

        }
        catch(Exception NoSuchElementException){
            return new ArrayList<Student>();
        }
    }

    
}
