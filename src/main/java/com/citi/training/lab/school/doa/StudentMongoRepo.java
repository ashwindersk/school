package com.citi.training.lab.school.doa;

import java.util.List;

import com.citi.training.lab.school.model.Student;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentMongoRepo extends MongoRepository<Student, String> {

    List<Student> findByName(String name);

}
