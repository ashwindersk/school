package com.citi.training.lab.school.doa;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.citi.training.lab.school.model.Teacher;

public interface TeacherMongoRepo extends MongoRepository<Teacher, String> {
    
}
