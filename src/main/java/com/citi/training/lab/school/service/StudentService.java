package com.citi.training.lab.school.service;

import java.util.List;


import com.citi.training.lab.school.doa.StudentMongoRepo;
import com.citi.training.lab.school.model.Student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentService {
    
    @Autowired
    private StudentMongoRepo mongoRepo;

    public List<Student> findAll(){
        return mongoRepo.findAll();
    }

    public boolean existsById(String id){
        return mongoRepo.existsById(id);
    }

    public Student save(Student student){
        return mongoRepo.save(student);
    }

    public Void deleteById(String id){
        mongoRepo.deleteById(id);
        return null;
    }

    public Student update(String id, Student student){
        student.setId(id);

        return mongoRepo.save(student);

    }
    

}
